package com.bse.configurator.company.hindi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author pushpa.gudmalwar
 * It is a CompanyHindi class.
 * Fields:
 * id[long] Auto-increment Id
 * scripId[String]
 * scripCode[String]
 * name[String]
 * abbrName[String]
 * grams[String] company grams
 * isinNumber[String]
 * status[Boolean]
 * startDate[Date]
 * endDate[Date]
 */
@Entity
@Table(name = "companies_hindi")
public class CompanyHindi {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "company_id")
	private long id;
	
	@org.springframework.data.annotation.Transient
	private Boolean requested;
	
	@Column(name = "company_scrip_id")
	private String scripId;

	@Column(name = "company_scrip_code")
	private String scripCode;

	@Column(name = "company_name")
	private String name;

	@Column(name = "company_abbr_name")
	private String abbrName;

	@NotNull
	@NotEmpty
	@Column(name = "company_grams")
	private String grams;
	
	@Column(name = "company_isin_number")
	private String isinNumber;

	@Column(name = "company_status")
	private Boolean status;

	private Date startDate = new Date();
	private Date endDate = null;
	
	private Date requestedDate = null;
	private Date approveDate = null;
	
	@Column(name="approved_by")
	private String approvedBy;
	
	@Column(name="requested_by")
	private String requestedBy;
	
	@Column(name="request_id")
	private long requestId;
	
	@Column(name="comment")
	private String comment;

	public CompanyHindi() {
		try {
			this.endDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("9999-12-31 23:59:59");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public CompanyHindi(CompanyHindi companyHindi,Boolean status) {
		this();
		this.status=status;
		this.scripId=companyHindi.getScripId();
		this.scripCode=companyHindi.getScripCode();
		this.name=companyHindi.getName();
		this.abbrName=companyHindi.getAbbrName();
		this.grams=companyHindi.getGrams();
		this.isinNumber=companyHindi.getIsinNumber();
	}
	
	public String getIsinNumber() {
		return isinNumber;
	}

	public void setIsinNumber(String isinNumber) {
		this.isinNumber = isinNumber;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getScripId() {
		return scripId;
	}

	public void setScripId(String scripId) {
		this.scripId = scripId;
	}

	public String getScripCode() {
		return scripCode;
	}

	public void setScripCode(String scripCode) {
		this.scripCode = scripCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbbrName() {
		return abbrName;
	}

	public void setAbbrName(String abbrName) {
		this.abbrName = abbrName;
	}

	public String getGrams() {
		return grams;
	}

	public void setGrams(String grams) {
		this.grams = grams;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public long getRequestId() {
		return requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Boolean getRequested() {
		return requested;
	}

	public void setRequested(Boolean requested) {
		this.requested = requested;
	}

}
