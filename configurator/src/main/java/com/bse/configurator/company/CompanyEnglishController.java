package com.bse.configurator.company;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import com.bse.configurator.util.Constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bse.configurator.email.Email;
import com.bse.configurator.email.EmailService;
import com.bse.configurator.exception.ResourceNotFoundException;
import com.bse.configurator.request.type.RequestType;
import com.bse.configurator.request.type.RequestTypeRepository;
import com.bse.configurator.requests.Request;
import com.bse.configurator.requests.RequestOperation;
import com.bse.configurator.requests.RequestRepository;

/**
 * 
 * @author bharat.pattani Company Details
 * 
 */
@RestController
@RequestMapping("/company/english")
public class CompanyEnglishController {
	@Autowired
	EmailService emailService;

	private Email email;

	CompanyEnglishRepository companyEngRepository;
	private RequestRepository requestRepository;
	private RequestTypeRepository requestTypeRepository;

	@Autowired
	public CompanyEnglishController(CompanyEnglishRepository companyEngRepository, RequestRepository requestRepository,
			RequestTypeRepository requestTypeRepository) {
		this.companyEngRepository = companyEngRepository;
		this.requestRepository = requestRepository;
		this.requestTypeRepository = requestTypeRepository;
		email = new Email();
	}

	@GetMapping()	
	public List<CompanyEnglish> getCompanies() {
			return this.companyEngRepository.findAllRecords(Constants.COMPANY_ENGLISH,Constants.GRAM);
	}

	public void upsertCompany(CompanyEnglish company, String username) {
		company.setApproveDate(new Date());
		company.setApprovedBy(username);
		company.setRequestedBy(username);
		this.companyEngRepository.save(company);
	}

	public void upsertCompany(CompanyEnglish company, String approvedBy, String requestedBy) {
		if (company.getRequestedBy() == null) {
			company.setApproveDate(new Date());
		}
		company.setApprovedBy(approvedBy);
		company.setRequestedBy(requestedBy);
		this.companyEngRepository.save(company);
	}

	/*
	 * @PutMapping("/{id}") public void updateGram(@Valid @RequestBody Company
	 * company, @PathVariable("id") Long id) { String username = (String)
	 * SecurityContextHolder.getContext().getAuthentication().getPrincipal(); String
	 * companyGrams = company.getGrams(); Company existingCompany =
	 * companyEngRepository.findById(id); if (existingCompany == null) { throw new
	 * ResourceNotFoundException((long) 400, "Company details not found."); } else {
	 * existingCompany.setEndDate(new Date());
	 * this.companyEngRepository.save(existingCompany); Company updatedCompany = new
	 * Company(existingCompany,true); updatedCompany.setGrams(companyGrams);
	 * this.upsertCompany(updatedCompany,username); this.email.setMessage(String.
	 * format("%s has changed %s company gram as '%s' at %s.",username,
	 * updatedCompany.getName(),updatedCompany.getGrams(),updatedCompany.
	 * getStartDate())); this.emailService.send(this.email); } }
	 */

	public void approveCompanyRequest(CompanyEnglish existingCompany, CompanyEnglish updatedCompany, String approvedBy,
			String requestedBy) {
		if (existingCompany == null) {
			throw new ResourceNotFoundException((long) 400, "Company details not found.");
		} else {
			existingCompany.setEndDate(new Date());
			this.companyEngRepository.save(existingCompany);
			this.upsertCompany(updatedCompany, approvedBy, requestedBy);
		}
	}

	@PutMapping("/{id}")
	public void updateGram(@Valid @RequestBody CompanyEnglish company, @PathVariable("id") Long id) {
		boolean authorized = false;
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String companyGrams = company.getGrams();
		CompanyEnglish existingCompany = companyEngRepository.findById(id);
		if (existingCompany == null) {
			throw new ResourceNotFoundException((long) 400, "Company details not found.");
		} else {
			if (authorized) {
				existingCompany.setEndDate(new Date());
				this.companyEngRepository.save(existingCompany);
				CompanyEnglish updatedCompany = new CompanyEnglish(existingCompany, true);
				updatedCompany.setRequestedBy(username);
				if (updatedCompany.getRequestedDate() == null) {
					updatedCompany.setRequestedDate(new Date());
				}
				updatedCompany.setGrams(companyGrams);
				this.upsertCompany(updatedCompany, username);
				RequestType requestType = this.requestTypeRepository.findBySourceType(Constants.GRAM, Constants.COMPANY_ENGLISH);	
				Request request = new Request(requestType, company.getGrams(),existingCompany.getGrams(), username, username,new Date(),new Date(),
						RequestOperation.UPDATE);
				this.requestRepository.save(request);
				
				this.email.setMessage(String.format("%s has changed %s company gram from '%s' to  '%s' at %s.", username,
						updatedCompany.getName(),existingCompany.getGrams(), updatedCompany.getGrams(), updatedCompany.getStartDate()));
				this.emailService.send(this.email);
			} else {
				String requestedValue = id + ":" + companyGrams;
				Date requestedDate = new Date();
				RequestType requestType = this.requestTypeRepository.findBySourceType(Constants.GRAM, Constants.COMPANY_ENGLISH);
				Request request = new Request(requestType, requestedValue, existingCompany.getGrams(), username,
						requestedDate, RequestOperation.UPDATE);// for other user
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has requested to change %s company gram from '%s' to '%s' at %s.", username,
						existingCompany.getName(), existingCompany.getGrams(), companyGrams, requestedDate));
				this.emailService.send(this.email);
			}
		}
	}

	@PutMapping("/inactive/{id}")
	public void deActivateSource(@PathVariable("id") Long id) {
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		CompanyEnglish company = this.companyEngRepository.findByActiveId(id);
		if (company == null) {
			throw new ResourceNotFoundException((long) 400, "Company details not found.");
		} else {
			company.setEndDate(new Date());
			this.companyEngRepository.save(company);
			CompanyEnglish updatedCompany = new CompanyEnglish(company, false);
			this.upsertCompany(updatedCompany, username);
			this.email.setMessage(String.format("%s has deactivated %s at %s.", username, updatedCompany.getName(),
					updatedCompany.getStartDate()));
			this.emailService.send(this.email);
		}
	}

	@PutMapping("/active/{id}")
	public void activateSource(@PathVariable("id") Long id) {
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		CompanyEnglish company = this.companyEngRepository.findByInactiveId(id);
		if (company == null) {
			throw new ResourceNotFoundException((long) 400, "Company details not found.");
		} else {
			company.setEndDate(new Date());
			this.companyEngRepository.save(company);
			CompanyEnglish updatedCompany = new CompanyEnglish(company, true);
			this.upsertCompany(updatedCompany, username);
			this.email.setMessage(String.format("%s has activated %s at %s.", username, updatedCompany.getName(),
					updatedCompany.getStartDate()));
			this.emailService.send(this.email);

		}
	}

}
