package com.bse.configurator.company.hindi;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bse.configurator.company.hindi.CompanyHindi;
import com.bse.configurator.company.hindi.CompanyHindiRepository;
import com.bse.configurator.email.Email;
import com.bse.configurator.email.EmailService;
import com.bse.configurator.exception.ResourceNotFoundException;
import com.bse.configurator.request.type.RequestType;
import com.bse.configurator.request.type.RequestTypeRepository;
import com.bse.configurator.requests.Request;
import com.bse.configurator.requests.RequestOperation;
import com.bse.configurator.requests.RequestRepository;
import com.bse.configurator.util.Constants;

/**
 * 
 * @author pushpa.gudmalwar Company Details
 * 
 */
@RestController
@RequestMapping("/company/hindi")
public class CompanyHindiController {

	@Autowired
	EmailService emailService;

	private Email email;

	CompanyHindiRepository companyHindiRepository;
	private RequestRepository requestRepository;
	private RequestTypeRepository requestTypeRepository;

	@Autowired
	public CompanyHindiController(CompanyHindiRepository companyHindiRepository, RequestRepository requestRepository,
			RequestTypeRepository requestTypeRepository) {
		this.companyHindiRepository = companyHindiRepository;
		this.requestRepository = requestRepository;
		this.requestTypeRepository = requestTypeRepository;
		email = new Email();
	}

	@GetMapping()	
	public List<CompanyHindi> getCompanies() {
		return this.companyHindiRepository.findAllRecords(Constants.COMPANY_HINDI,Constants.GRAM);
	}

	public void upsertCompany(CompanyHindi companyHindi, String username) {
		companyHindi.setApproveDate(new Date());
		companyHindi.setApprovedBy(username);
		companyHindi.setRequestedBy(username);
		this.companyHindiRepository.save(companyHindi);
	}

	public void upsertCompany(CompanyHindi companyHindi, String approvedBy, String requestedBy) {
		if (companyHindi.getRequestedBy() == null) {
			companyHindi.setApproveDate(new Date());
		}
		companyHindi.setApprovedBy(approvedBy);
		companyHindi.setRequestedBy(requestedBy);
		this.companyHindiRepository.save(companyHindi);
	}

	public void approveCompanyRequest(CompanyHindi existingCompany, CompanyHindi updatedCompany, String approvedBy,
			String requestedBy) {
		if (existingCompany == null) {
			throw new ResourceNotFoundException((long) 400, "Company details not found.");
		} else {
			existingCompany.setEndDate(new Date());
			this.companyHindiRepository.save(existingCompany);
			this.upsertCompany(updatedCompany, approvedBy, requestedBy);
		}
	}

	@PutMapping("/{id}")
	public void updateGram(@Valid @RequestBody CompanyHindi companyHindi, @PathVariable("id") Long id) {
		boolean authorized = false;
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String companyGrams = companyHindi.getGrams();
		CompanyHindi existingCompany = companyHindiRepository.findById(id);
		if (existingCompany == null) {
			throw new ResourceNotFoundException((long) 400, "Company details not found.");
		} else {
			if (authorized) {
				existingCompany.setEndDate(new Date());
				this.companyHindiRepository.save(existingCompany);
				CompanyHindi updatedCompany = new CompanyHindi(existingCompany, true);
				updatedCompany.setRequestedBy(username);
				if (updatedCompany.getRequestedDate() == null) {
					updatedCompany.setRequestedDate(new Date());
				}
				updatedCompany.setGrams(companyGrams);
				this.upsertCompany(updatedCompany, username);
				RequestType requestType = this.requestTypeRepository.findBySourceType(Constants.GRAM, Constants.COMPANY_HINDI);	
				Request request = new Request(requestType, companyHindi.getGrams(),existingCompany.getGrams(), username, username,new Date(),new Date(),
						RequestOperation.UPDATE);
				this.requestRepository.save(request);
				
				this.email.setMessage(String.format("%s has changed %s company gram from '%s' to  '%s' at %s.", username,
						updatedCompany.getName(),existingCompany.getGrams(), updatedCompany.getGrams(), updatedCompany.getStartDate()));
				this.emailService.send(this.email);
			} else {
				String requestedValue = id + ":" + companyGrams;
				Date requestedDate = new Date();
				RequestType requestType = this.requestTypeRepository.findBySourceType(Constants.GRAM, Constants.COMPANY_HINDI);
				Request request = new Request(requestType, requestedValue, existingCompany.getGrams(), username,
						requestedDate, RequestOperation.UPDATE);// for other user
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has requested to change %s company gram from '%s' to '%s' at %s.", username,
						existingCompany.getName(), existingCompany.getGrams(), companyGrams, requestedDate));
				this.emailService.send(this.email);
			}
		}
	}

	@PutMapping("/inactive/{id}")
	public void deActivateSource(@PathVariable("id") Long id) {
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		CompanyHindi companyHindi = this.companyHindiRepository.findByActiveId(id);
		if (companyHindi == null) {
			throw new ResourceNotFoundException((long) 400, "Company details not found.");
		} else {
			companyHindi.setEndDate(new Date());
			this.companyHindiRepository.save(companyHindi);
			CompanyHindi updatedCompany = new CompanyHindi(companyHindi, false);
			this.upsertCompany(updatedCompany, username);
			this.email.setMessage(String.format("%s has deactivated %s at %s.", username, updatedCompany.getName(),
					updatedCompany.getStartDate()));
			this.emailService.send(this.email);
		}
	}

	@PutMapping("/active/{id}")
	public void activateSource(@PathVariable("id") Long id) {
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		CompanyHindi companyHindi = this.companyHindiRepository.findByInactiveId(id);
		if (companyHindi == null) {
			throw new ResourceNotFoundException((long) 400, "Company details not found.");
		} else {
			companyHindi.setEndDate(new Date());
			this.companyHindiRepository.save(companyHindi);
			CompanyHindi updatedCompany = new CompanyHindi(companyHindi, true);
			this.upsertCompany(updatedCompany, username);
			this.email.setMessage(String.format("%s has activated %s at %s.", username, updatedCompany.getName(),
					updatedCompany.getStartDate()));
			this.emailService.send(this.email);

		}
	}


}
