package com.bse.configurator.security;

import com.bse.configurator.user.ApplicationUser;
import com.bse.configurator.user.ApplicationUserAudit;
import com.bse.configurator.user.ApplicationUserAuditRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static com.bse.configurator.security.SecurityConstants.EXPIRATION_TIME;
import static com.bse.configurator.security.SecurityConstants.HEADER_STRING;
import static com.bse.configurator.security.SecurityConstants.SECRET;
import static com.bse.configurator.security.SecurityConstants.TOKEN_PREFIX;
import static com.bse.configurator.security.SecurityConstants.EXPOSE_HEADER_STRING;
import static com.bse.configurator.security.SecurityConstants.EXPOSE_ROLE;
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	private AuthenticationManager authenticationManager;
	private ApplicationUserAuditRepository applicationUserAuditRepository;
	
	public JWTAuthenticationFilter(AuthenticationManager authenticationManager,ApplicationUserAuditRepository applicationUserAuditRepository) {
		this.authenticationManager = authenticationManager;
		this.applicationUserAuditRepository = applicationUserAuditRepository;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
			throws AuthenticationException {
		try {
			ApplicationUser creds = new ObjectMapper().readValue(req.getInputStream(), ApplicationUser.class);
			return authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(creds.getUsername(), creds.getPassword(),
							(Collection<? extends GrantedAuthority>) new ArrayList<SimpleGrantedAuthority>()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		String username = ((User) auth.getPrincipal()).getUsername();
		String role = auth.getAuthorities().toString();
		String ipAddress = req.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = req.getRemoteAddr();
		}
		ApplicationUserAudit applicationUserAudit = new ApplicationUserAudit(username,ipAddress);
		this.applicationUserAuditRepository.save(applicationUserAudit);
		String token = Jwts.builder().setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.setAudience(role)
				.signWith(SignatureAlgorithm.HS512, SECRET.getBytes()).compact();
		res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
		res.addHeader(EXPOSE_HEADER_STRING, HEADER_STRING);
		res.addHeader(EXPOSE_ROLE, role);

	}
}