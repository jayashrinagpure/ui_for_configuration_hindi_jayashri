package com.bse.configurator.source.twitter;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bse.configurator.source.SourceType;
import com.bse.configurator.source.twitter.TwitterSource;
@Repository
public interface TwitterSourceRepository extends JpaRepository<TwitterSource, Long>{
	@Query(nativeQuery=true,value="SELECT CASE WHEN t2.existing_value IS NULL THEN false ELSE true END as requested,t1.* from (SELECT s.source_type as source_type,tw.* FROM public.twitter_source tw INNER JOIN source_type s ON tw.twitter_source_type_id = s.id) t1 LEFT OUTER JOIN (SELECT rt.request_media,rt.request_type,r.* from public.request r INNER JOIN public.request_type rt ON r.request_type_id = rt.id where rt.request_media='twitter' and rt.request_type in ('handle','hashtag') and r.request_status='PENDING') t2 ON t1.twitter_source_name=t2.existing_value AND t1.source_type=t2.request_type where t1.end_date='9999-12-31 23:59:59'")
	List<TwitterSource> findAllRecords();
	
	@Query(nativeQuery=true,value="SELECT CASE WHEN t2.existing_value IS NULL THEN false ELSE true END as requested,t1.* from (SELECT s.source_type as source_type,tw.* FROM public.twitter_source tw INNER JOIN source_type s ON tw.twitter_source_type_id = s.id) t1 LEFT OUTER JOIN (SELECT rt.request_media,rt.request_type,r.* from public.request r INNER JOIN public.request_type rt ON r.request_type_id = rt.id where rt.request_media='twitter' and rt.request_type in ('handle','hashtag') and r.request_status='PENDING') t2 ON t1.twitter_source_name=t2.existing_value AND t1.source_type=t2.request_type where t1.end_date='9999-12-31 23:59:59'  and  t1.twitter_source_status = ?1")
	List<TwitterSource> findRecordsByStatus(Boolean status);

	@Query("select t from TwitterSource t where t.endDate='9999-12-31 23:59:59' and t.sourceType=?1  and t.sourceName=?2")
	TwitterSource doesSourceExists(SourceType sourceType,String sourceName);
	
	@Query("select t from TwitterSource t where t.endDate='9999-12-31 23:59:59' and t.id = ?1 and t.sourceType=?2 and t.status = true and t.sourceName=?3")
	TwitterSource findByActiveSource(Long id,SourceType sourceType,String sourceName);
	
	@Query("select t from TwitterSource t where t.endDate='9999-12-31 23:59:59' and t.id = ?1 and t.sourceType=?2 and t.status = false and t.sourceName=?3")
	TwitterSource findByInactiveSource(Long id,SourceType sourceType,String sourceName);

}
