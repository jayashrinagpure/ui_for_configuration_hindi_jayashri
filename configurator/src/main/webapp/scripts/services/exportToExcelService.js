app.service('exportToExcelService', function () {
	this.converttoExcel = function (csvString,fileName) {		
     	var excelFile = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:xls' xmlns='http://www.w3.org/TR/REC-html40'>";
     	excelFile += "<head>";
     	excelFile += "<!--[if gte mso 9]>";
     	excelFile += "<xml>";
     	excelFile += "<x:ExcelWorkbook>";
     	excelFile += "<x:ExcelWorksheets>";
     	excelFile += "<x:ExcelWorksheet>";
     	excelFile += "<x:Name>";
     	excelFile += "{worksheet}";
     	excelFile += "</x:Name>";
     	excelFile += "<x:WorksheetOptions>";
     	excelFile += "<x:DisplayGridlines/>";
     	excelFile += "</x:WorksheetOptions>";
     	excelFile += "</x:ExcelWorksheet>";
     	excelFile += "</x:ExcelWorksheets>";
     	excelFile += "</x:ExcelWorkbook>";
     	excelFile += "</xml>";
     	excelFile += "<![endif]-->";
     	excelFile += "</head>";
     	excelFile += "<body>";
     	excelFile += csvString;
     	excelFile += "</body>";
     	excelFile += "</html>";
     	var fileType = "xls";
		var blob = new Blob([excelFile], { type: 'text/' + fileType });

     	var a = $('<a/>', {
            style:'display:none',
            href:window.URL.createObjectURL(blob, { type: "text/plain" }),
            download:fileName+ new Date().toDateString() +'.xls'
        }).appendTo('body');
        a[0].click()
        a.remove();
}});