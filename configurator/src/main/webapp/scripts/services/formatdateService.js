app.service('formatdateService', function ($filter) {	
	 this.formatdate = function(_List){
		_List.forEach(function(x){
			if(x.approveDate){
	    		x.approveDate = $filter('date')(x.approveDate, 'EEE-yyyy-MM-dd HH:mm:ss');
			}
			if(x.requestedDate){
	    		x.requestedDate = $filter('date')(x.requestedDate, 'EEE-yyyy-MM-dd HH:mm:ss');
			}
			if(x.actionedDate){
	    		x.actionedDate = $filter('date')(x.actionedDate, 'EEE-yyyy-MM-dd HH:mm:ss');
			}
		   })
		   return _List;
	}
});