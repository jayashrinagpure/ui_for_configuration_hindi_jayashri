/**
 * @author Jayashri Nagpure
 * @name webApp.service: getRumoursVideoListService
 * @description
 * #getRumoursVideoListService
 * It is service to get rumours video list
 * This service is responsible for getting processed rumours video list.
 */

app.service('getRumoursVideoListService', function ($http,$q) {
    this.getVideoList = function() {
        var deferred = $q.defer();
        $http({
                method: 'POST',
                url: 'http://13.233.79.216:5004/video_list'
            })
            .then(function(data){
                deferred.resolve(data);
            },function(data){
            	 errorToasty("Failed to load data");
                 deferred.reject(data)
            });
        return deferred.promise;
    };
});