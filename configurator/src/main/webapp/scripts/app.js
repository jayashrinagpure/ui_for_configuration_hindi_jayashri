	var app = angular.module('webApp', ['ui.router','ngIdle','http-auth-interceptor','angular-toasty','LocalStorageModule','ngFlash','dirPagination','mp.datePicker','ngSanitize','com.2fdevs.videogular','ngFileUpload'])
	.config(function($stateProvider, $urlRouterProvider,$qProvider,$locationProvider) {
		$locationProvider.hashPrefix('');
		 $qProvider.errorOnUnhandledRejections(false);
		$stateProvider
		.state('signup', {
			url: '/signup',
			templateUrl: 'scripts/app/signup/signup.html',
			controller: 'signupController'
		})
		.state('login', {
			url: '/login',
			templateUrl: 'scripts/app/login/login.html',
			controller: 'loginController'
		})
		.state('reset-password', {
			url: '/reset-password',
			templateUrl: 'scripts/app/login/forgotPassword.html',
			controller: 'resetPasswordController'
		})
		.state('changePassword', {
			url: '/changePassword',
			templateUrl: 'scripts/app/login/changePassword.html',
			controller: 'changePasswordController'
		})
		.state('parentRequest', {
			url: '/request',
			templateUrl: 'scripts/app/Notification/parentRequest.html',
			controller: 'parentRequestController'	
		})
		.state("parentRequest.all",{
			url: '/all',
			templateUrl: 'scripts/app/Notification/allRequest.html',
			controller:'allRequestController'
		})
		.state("parentRequest.pending",{
			url: '/pending',
			templateUrl: 'scripts/app/Notification/pendingRequest.html',
			controller:'pendingRequestController'
		})	
		.state("parentRequest.approved",{
			url: '/approved',
			templateUrl: 'scripts/app/Notification/approvedRequest.html',
			controller:'approvedRequestController'
		})	
		.state("parentRequest.rejected",{
			url: '/rejected',
			templateUrl: 'scripts/app/Notification/rejectedRequest.html',
			controller:'rejectedRequestController'
		})
		.state("userAudit",{
			url: '/userAudit',
			templateUrl: 'scripts/app/Notification/userAudit.html',
			controller:'userAuditController'
		})
		.state('logout', {
			url: '/logout',
			controller: 'logoutController'
		})
		.state('homeParent', {
			url: '/',
			templateUrl: 'scripts/app/home/home.html',
			controller: 'homeController',
			resolve: {

			},
			abstract : true
		})
		.state("homeParent.home",{
			url: 'home',
			templateUrl: 'scripts/app/home/homePage.html',
			controller:'homeChildController'
		})

		//company
		.state("homeParent.company",{
			url: 'company/english',
			templateUrl: 'scripts/app/company/templates/company.html',
			controller: 'companyController'
		})
		.state("homeParent.company_hindi",{
			url: 'company/hindi',
			templateUrl: 'scripts/app/company/templates/company_hindi.html',
			controller: 'company_hindiController'
		})

        //sources
        .state("homeParent.webSource",{
        	url: 'webSource/english',
        	templateUrl: 'scripts/app/sources/templates/webSource.html',
        	controller: 'webSourceController'
        })
        .state("homeParent.twitterSource",{
        	url:"twitterSource/english/:status",
        	params: {status: 'all'},
        	templateUrl: 'scripts/app/sources/templates/twitterSource.html',
        	controller: 'twitterSourceController'
        })
        .state("homeParent.facebookSource",{
        	url:"facebookSource/english/:status",
        	params: {status: 'all'},
        	templateUrl: 'scripts/app/sources/templates/facebookSource.html',
        	controller: 'facebookSourceController'
        })
        .state("homeParent.webSource_hindi",{
        	url: 'webSource/hindi',
        	templateUrl: 'scripts/app/sources/templates/webSource_hindi.html',
        	controller: 'webSource_hindiController'
        })
 
	//keywords
	.state("homeParent.webKeyword",{
		url: 'webKeyword/english/:status',
		params: {status: 'all'},
		templateUrl: 'scripts/app/keywords/templates/webKeyword.html',
		controller: 'webKeywordController'
	})
	.state("homeParent.twitterKeyword",{
		url: "twitterKeyword/english/:status",
		params: {status: 'all'},
		templateUrl: 'scripts/app/keywords/templates/twitterKeyword.html',
		controller: 'twitterKeywordController'
	})
	.state("homeParent.facebookKeyword",{
		url : 'facebookKeyword/english/:status',
		params: {status: 'all'},
		templateUrl: 'scripts/app/keywords/templates/facebookKeyword.html',
		controller: 'facebookKeywordController'
	})
	.state("homeParent.webKeyword_hindi",{
		url: 'webKeyword/hindi/:status',
		params: {status: 'all'},
		templateUrl: 'scripts/app/keywords/templates/webKeyword_hindi.html',
		controller: 'webKeyword_hindiController'
	})
	//video to text
    .state('homeParent.videoToText', {
        url: 'videototext',
        templateUrl: 'scripts/app/videototext/videototext.html',
        controller: 'videototextController'
    }) 
    .state('homeParent.rumoursVideo', {
        url: 'rumoursVideo',
        templateUrl: 'scripts/app/videototext/rumours_video/rumoursVideo.html',
        controller: 'rumoursVideoController'
    })         

	$urlRouterProvider.otherwise('home');
});

// omitted for brevity
app.config(function(IdleProvider, KeepaliveProvider) {
  IdleProvider.idle(10*60); // 10 minutes idle  10*60
  IdleProvider.timeout(30); // after 30 seconds idle, time the user out
})

/**
 * CompilerProvider to download text file. 
*/
app.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(|blob|):/);
}]);

/**
 * constant for youtube player
 *
 */

app.constant('YT_event', {
	  STOP:            0, 
	  PLAY:            1,
	  PAUSE:           2,
	  STATUS_CHANGE:   3
	});

app.run(function($state,$rootScope,localStorageService,$location,$window, $http,Idle){
	'use strict';
    $rootScope.globals = localStorageService.get('globals');
    if($rootScope.globals){
    var authToken = $rootScope.globals.currentUser.authdata;
    $http.defaults.headers.common.Authorization = authToken;
    }
    $rootScope.$on('IdleTimeout', function() {
    	$state.go('logout');
    	
    	Idle.watch();
    });
    	// start watching when the app runs. also starts the Keepalive service by default.
    Idle.watch();
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
    	if(localStorageService.get('authorizationData')){
    		var user_role = localStorageService.get('authorizationData').user_role.replace(/[^a-zA-Z0-9 ]/g, "").toLowerCase(); 
    		$rootScope.userName = localStorageService.get('authorizationData').userName;
         	// getting user role here and setting global variable 	    		
        	if(user_role == 'super user'){
        		$rootScope.userRole ='super_user' ;
        		$rootScope.isSuperUser = true;
        		$rootScope.permissions = true;
        	}else if(user_role == 'user'){
        		$rootScope.userRole ='user' ;
        		$rootScope.isSuperUser = false;
        		$rootScope.permissions = true;
        	}else if(user_role == 'viewer'){
        		$rootScope.userRole ='viewer' ;
        		$rootScope.isSuperUser = false;
        		$rootScope.permissions = false;
        	}
    	}
	
        if($rootScope.globals){
    	var userName= $rootScope.globals.currentUser.username; 
       // $rootScope.permissions = true;	
    	    		
    		        var loggedIn= $rootScope.globals.currentUser.authdata;
    		        if (!loggedIn) {
    		        	$state.go('login');
    		        } 
        }
    		 
    });
    
 // Call when the the client is confirmed
    $rootScope.$on('event:auth-loginConfirmed', function(data) {
        $rootScope.authenticated = true;
    console.log('loginConfirmed');
    $location.path('/home').replace();
        if ($location.path() === "/login") {
    	$location.path('/home').replace();
    } else {
           //not login page
        }
    }); 
    // Call when the the client is confirmed
    $rootScope.$on('event:auth-loginCancelled', function() {
        $rootScope.authenticated = false;
    console.log('loginCancelled');
    $state.go('logout');
    }); 
    
    $rootScope.$on('event:auth-loginRequired', function(data) {
        console.log('loginRequired');
		$http.defaults.headers.common.Authorization=null;
		$state.go("login");
        }); 
    $rootScope.$on('event:auth-forbidden', function(data) {
        console.log('forbidden');
        $state.go('logout');
        
        }); 
});