app.controller('pendingRequestController',function($state,Flash,$scope,$http,$filter,exportToExcelService,sortDataService) {
	    $scope.menu.current="pending";
	    
		//sorting column 
		$scope.sortColumn = "id";
		$scope.reverseSort = false;

		$scope.message = "Pending Request Page";
		//var PendingRequestList;
		$scope.rowLimitOptionArray = [10,20,30,50,100];  
		$scope.selected = {rowLimit:$scope.rowLimitOptionArray[1]};
		
		/**
		 * Sets pending request list 
		 * response:successful rest call response contains pending request data
		 */
		var successCallBack = function (response) {
			$scope.allRequestList = response.data;
			$scope.allRequestList.forEach(function (element) {
				if (element.requestType.requestMedia == 'all_english' || element.requestType.requestMedia == 'all_hindi') {
					element.companyrequestedValue = element.requestedValue.substring(element.requestedValue.indexOf(':') + 1, element.requestedValue.length);
				}
				if (element.requestedOperation == 'ADD') {
					element.addexistingValue = '';
				}
			})

			$scope.exportInfo = angular.copy(response.data);
		};

		/**
		 * Sets error 
		 * reason:error information
		 */
		var errorCallBack = function (reason) {
			$scope.error = reason.data;
		};
		
		/**
		 * Get pending request data 
		 * status:status of request(all,pending,approved,rejected)
		 */
		getAllRequestList = function (status) {
			var _url;
			if (status == 'pending')
				_url = 'request/pending';
			$http({
					method: "GET",
					url: _url
				})
				.then(successCallBack, errorCallBack);
		};
		

		/**
		 * Approving request
		 * request:current request
		 */
	$scope.acceptRequest = function(request){
		//request.status = true;
		var _url;

		if (request.requestedOperation == 'ADD') {
			_url = 'request/pending/' + request.id;
		} else
		if (request.requestedOperation == 'ACTIVE') {
			_url = 'request/pending/active/' + request.id;
		} else
		if (request.requestedOperation == 'INACTIVE') {
			_url = 'request/pending/inactive/' + request.id;
		} else
		if (request.requestedOperation == 'UPDATE' && request.requestType.requestMedia == 'all_english') {
			_url = 'request/pending/company/english/' + request.id;
		} else
		if (request.requestedOperation == 'UPDATE' && request.requestType.requestMedia == 'all_hindi') {
			_url = 'request/pending/company/hindi/' + request.id;
		}
		var acceptingRequestData = {
			"requestedValue": request.requestedValue,
			"requestType": {
				"id": request.requestType.id,
				"requestType": request.requestType.requestType,
				"requestMedia": request.requestType.requestMedia
			},
			"comment": request.comment
		};

		$http({
			method: "PUT",
			url: _url,
			data: acceptingRequestData
		}).then(function (response) {
			getAllRequestList('pending');
			var message = '<strong>' + request.requestedValue.toUpperCase() + '</strong> is approved successfully';
			var id = Flash.create('success', message);
			//$scope.getAcceptedRequest();
		}, function (reason) {
			var message = '<strong>' + request.requestedValue.toUpperCase() + '</strong> is unable to approve, might be due to duplicates.';
			var id = Flash.create('danger', message);
			$scope.error = reason.data;
		});

	};

	/**
	 * Rejecting request
	 * request:current request
	 */
	$scope.rejectRequest = function (request) {
		var rejectingRequestData = {
			"requestedValue": request.requestedValue,
			"requestType": {
				"id": request.requestType.id,
				"requestType": request.requestType.requestType,
				"requestMedia": request.requestType.requestMedia
			},
			"comment": request.comment
		};
		$http({
			method: "PUT",
			url: 'request/pending/rejected/' + request.id,
			data: rejectingRequestData
		}).then(function (response) {
			getAllRequestList('pending');
			var message = '<strong>' + request.requestedValue.toUpperCase() + '</strong> is rejected successfully';
			var id = Flash.create('success', message);
		}, function (reason) {
			var message = '<strong>' + request.requestedValue.toUpperCase() + '</strong> is unable to reject';
			var id = Flash.create('danger', message);
			$scope.error = reason.data;
		});
	};


	//On load dispaly ALL data
	getAllRequestList('pending');

	/**
	 * Exports to Excel
	 */
    $scope.export = function(){
    	var table = $scope.exportInfo;
    	 table = $filter('orderBy')(table, 'id');
        	var csvString = '<table><tr><td>Request Id</td><td>Requested By</td><td>Requested Operation</td><td>Existing Value</td><td>Requested Value</td><td>Media Type</td><td>Category</td></tr>';
        	for(var i=0; i<table.length;i++){
        		var rowData = table[i];
        		if(rowData.requestedOperation == 'ADD'){
        			rowData.existingValue = rowData.addexistingValue;
        		}
        		if(rowData.requestType.requestMedia == 'all_english' || rowData.requestType.requestMedia == 'all_hindi'){
        			rowData.requestedValue = rowData.companyrequestedValue
        		}

        		csvString = csvString + "<tr><td>" +rowData.id + "</td><td>" + rowData.requestedBy+"</td><td>" 
        		 + rowData.requestedOperation+ "</td><td>"+ rowData.existingValue + "</td><td>"+ rowData.requestedValue + "</td><td>" +rowData.requestType.requestType + "</td><td>"+rowData.requestType.requestMedia+"</td>";
        		csvString = csvString + "</tr>";
    	    }
        csvString += "</table>";
     	csvString = csvString.substring(0, csvString.length);
     	exportToExcelService.converttoExcel(csvString,'Pending_Requests');	        
    }

	//sorting column data after click on column name
	$scope.sortData = function (column) {
		sortDataService.sortData(column,$scope.sortColumn,$scope.reverseSort);
		$scope.reverseSort = sortDataService.sortDataServiceObject.reverseSort;
		$scope.sortColumn = sortDataService.sortDataServiceObject.sortColumn;
	}

	//getting column name to sort data
	$scope.getSortClass = function (column) {
		return sortDataService.getSortClass(column,$scope.sortColumn,$scope.reverseSort);
	}

/**
 * Exports to pdf
 */
    $scope.exportpdf = function () {
    	{
    		var item = $scope.exportInfo;
    		item = $filter('orderBy')(item, 'id');
    	    var doc = new jsPDF('l','cm',[30,30]); 
     	    var col = ["Request Id","Requested By","Requested Operation","Existing Value","Requested Value","Media Type","Category"];
     	    var rows = [];
    	    var dateInfo,statusInfo;	    	    
    	    for(var i=0; i<item.length;i++){
        		var rowData = item[i];
        		if(rowData.requestedOperation == 'ADD'){
        			rowData.existingValue = rowData.addexistingValue;
        		}
        		if(rowData.requestType.requestMedia == 'all_english' || rowData.requestType.requestMedia == 'all_hindi'){
        			rowData.requestedValue = rowData.companyrequestedValue
        		}

            	var	temp = [rowData.id, rowData.requestedBy, rowData.requestedOperation, rowData.existingValue, rowData.requestedValue, rowData.requestType.requestType, rowData.requestType.requestMedia];
         		rows.push(temp);
    	    }
    	   doc.autoTable(col, rows);
    	   doc.save('Pending_Requests'+ new Date().toDateString() +'.pdf');
    	  }
    };
	
	});

