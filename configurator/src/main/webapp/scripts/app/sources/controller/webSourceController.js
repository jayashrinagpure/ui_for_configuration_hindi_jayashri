app.controller("webSourceController", function($rootScope, $scope, $http,$filter,exportToExcelService,sortDataService) {
	var webSourceList;
	$scope.menu.current="web";

	// to heighlight web menu setting current menu as web
	$scope.menu.current = "web";

	//sorting column 
	$scope.sortColumn = "site";
	$scope.reverseSort = false;

	// page range selection options	
	$scope.rowLimitOptionArray = [10, 20, 30, 50, 100];
	$scope.selected = {
		rowLimit: $scope.rowLimitOptionArray[1]
	};

	/**
	 * Sets web source list 
	 * response:successful rest call response conatins web source data
	 */
	var successCallBack = function (response) {
		$scope.webSourceList = response.data;
		$scope.exportInfo = angular.copy(response.data);
	}

	/**
	 * Sets error 
	 * reason:error information
	 */
	var errorCallBack = function (reason) {
		$scope.error = reason.data;
	}

	/**
	 * Get web source data 
	 * status:status of source(active,inactive,all)
	 */
	getSourceList = function () {
		$http({
				method: "GET",
				url: 'source/web/english'
			})
			.then(successCallBack, errorCallBack);
	}

	getSourceList(); // on load 

	//sorting column data after click on column name
	$scope.sortData = function (column) {
		sortDataService.sortData(column,$scope.sortColumn,$scope.reverseSort);
		$scope.reverseSort = sortDataService.sortDataServiceObject.reverseSort;
		$scope.sortColumn = sortDataService.sortDataServiceObject.sortColumn;
	}

	//getting column name to sort data
	$scope.getSortClass = function (column) {
		return sortDataService.getSortClass(column,$scope.sortColumn,$scope.reverseSort);
	}
	
	/**
	 * Exports to Excel
	 */	
    $scope.export = function(){
    	var table = $scope.exportInfo;
    	 table = $filter('orderBy')(table, 'link');
    	var csvString = '<table><tr><td>RSS LINK</td><td>WEBSITE<td></tr>';
    	for(var i=0; i<table.length;i++){
    		var rowData = table[i];
    		var displayUrl;
    		if(rowData.htmlUrl == null){
    			displayUrl = rowData.site;
    		}
    		else {
    			displayUrl = rowData.htmlUrl;
    		}
    		csvString = csvString + "<tr><td>" +rowData.link + "</td><td>" +displayUrl+"</td>";
    		csvString = csvString + "</tr>";
	    }
    	csvString += "</table>";
     	csvString = csvString.substring(0, csvString.length);
     	exportToExcelService.converttoExcel(csvString,'Web_Source');	        
   }

	/**
	 * Exports to pdf
	*/   
    $scope.exportpdf = function (){
    	//endDocument();
    	    var item = $scope.exportInfo;
    	    var doc = new jsPDF(); 
    	   
    	    var col = ["RSS LINK","WEBSITE"];
    	    var rows = [];
    	    item = $filter('orderBy')(item, 'link');
    	    for(var i=0; i<item.length;i++){
        		var rowData = item[i];
        		var displayUrl;
        		if(rowData.htmlUrl == null){
        			displayUrl = rowData.site;
        		}
        		else {
        			displayUrl = rowData.htmlUrl;
        		}
        		var temp = [rowData.link, displayUrl];
        		rows.push(temp);
    	    }    	
    	   doc.autoTable(col, rows);
    	   doc.save('Web_Source'+ new Date().toDateString() +'.pdf');
    };

});