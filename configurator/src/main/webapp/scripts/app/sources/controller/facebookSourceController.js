app.controller("facebookSourceController", function ($rootScope,$scope, $http, $state, Flash, $stateParams,$filter,exportToExcelService,formatdateService,sortDataService) {
	$scope.menu.current="facebook";
	
	//sorting column 
	$scope.sortColumn = "sourceName";
	$scope.reverseSort = false;

	//OptionValues	
	$scope.rowLimitOptionArray = [10, 20, 30, 50, 100];
	$scope.selected = {
		rowLimit: $scope.rowLimitOptionArray[1]
	};

	/**
	 * Sets facebook source list 
	 * response:successful rest call response conatins facebook source data
	 */
	var successCallBack = function (response) {
		$scope.facebookSourceList = response.data;
		$scope.facebookSourceList = formatdateService.formatdate(response.data);
		$scope.exportInfo = angular.copy(response.data);
	}

	/**
	 * Sets error 
	 * reason:error information
	 */
	var errorCallBack = function (reason) {
		$scope.error = reason.data;
	}

	/**
	 * Get facebook source data 
	 * status:status of source(active,inactive,all)
	 */
	getSourceList = function (status) {
		var _url;
		if (status == 'active')
			_url = 'source/facebook/active';
		else if (status == 'inactive')
			_url = 'source/facebook/inactive';
		else
			_url = 'source/facebook';
		$http({
				method: "GET",
				url: _url
			})
			.then(successCallBack, errorCallBack);
	}
	getSourceList($stateParams.status); //on load
	$scope.orderStatus = $stateParams.status;

	/**
	 * Changes state according to status
	 * status:status of source(active,inactive,all) 
	 */
	$scope.orderByStatus = function (status) {
		$state.go('homeParent.facebookSource', {
			status: $scope.orderStatus
		});
	}

	/**
	 * Resets fields from add pop up
	 */
	$scope.reset = function () {
		$scope.facebookSourceType = null;
		$scope.facebookSourceName = null;
		$scope.confirmSubmit = null;
	}

	/**
	 * Adds source
	 * _Name: source name
	 * _Type: source type
	 */
	$scope.addingSource = function (_Name, _Type) {
		var facebookSource = {
			"sourceName": _Name,
			"sourceTypeName": _Type
		};
		$http({
				method: "POST",
				url: 'source/facebook',
				data: facebookSource
			})
			.then(function (response) {
				getSourceList($stateParams.status);
				// user role implementation messages
				if ($rootScope.userRole == 'super_user') {
					var message = '<strong>' + _Name.toUpperCase() + '</strong> is successfully added';
					var id = Flash.create('success', message);
				} else if ($rootScope.userRole == 'user') {
					var message = '<strong>' + _Name.toUpperCase() + '</strong> addition is pending for approval';
					var id = Flash.create('success', message);
				}

			}, function (reason) {
				var message = '<strong>' + _Name.toUpperCase() + '</strong> is unable to add , might be due to duplicates.';
				var id = Flash.create('danger', message);
				$scope.error = reason.data;
			}); //add
		$('#facebookSourceModal').modal('hide');
	}

	/**
	 * Inactivating source status
	 * _source: current source details
	 */
	$scope.inactiveStatus = function (_source) {
		_source.disabled = true
		var facebookSource = {
			"sourceName": _source.sourceName,
			"sourceTypeName": _source.sourceTypes.sourceType
		};
		$http({
			method: "PUT",
			url: 'source/facebook/inactive/' + _source.id,
			data: facebookSource
		}).then(function (response) {
			getSourceList($stateParams.status);
			// user role implementation messages
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is succesfully deactivated';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> deactivation is pending for approval ';
				var id = Flash.create('success', message);
			}

		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is unable to deactivate';
			var id = Flash.create('danger', message);
		});
	};

	/**
	 * Activating source status
	 * _source: current source details
	 */
	$scope.activateStatus = function (_source) {
		_source.disabled = true
		var facebookSource = {
			"sourceName": _source.sourceName,
			"sourceTypeName": _source.sourceTypes.sourceType
		};
		$http({
			method: "PUT",
			url: 'source/facebook/active/' + _source.id,
			data: facebookSource
		}).then(function (response) {
			getSourceList($stateParams.status);
			// user role implementation messages
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is succesfully activated';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> activation is pending for approval ';
				var id = Flash.create('success', message);
			}

		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is unable to activate';
			var id = Flash.create('danger', message);
		});
	};

	/**
	 * Exports to Excel
	 */	
    $scope.export = function(){
	    var table = $scope.exportInfo;
   	 table = $filter('orderBy')(table, 'sourceName');
    	var csvString = '<table><tr><td>Source</td><td>Type</td><td>Created By</td><td>Last Modified At</td><td>Status</td></tr>';
    	var dateInfo,statusInfo;
    	for(var i=0; i<table.length;i++){
    		var rowData = table[i];
    		var d = new Date();
    		d.setTime(rowData.startDate);
    		dateInfo = moment(d).format("MMM YYYY-MM-DD HH:mm:ss ");
    		if(rowData.status == true)
    			statusInfo = 'Active';
    		else
    			statusInfo = 'Inactive';
    		csvString = csvString + "<tr><td>" +rowData.sourceName + "</td><td>" + rowData.sourceTypes.sourceType+"</td><td>" + rowData.requestedBy+"</td><td>" + dateInfo+"</td><td>" + statusInfo+"</td>";
    		csvString = csvString + "</tr>";
	    }
    	csvString += "</table>";
     	csvString = csvString.substring(0, csvString.length);
     	exportToExcelService.converttoExcel(csvString,'Facebook_Source');	        
     }
    
	//sorting column data after click on column name
	$scope.sortData = function (column) {
		sortDataService.sortData(column,$scope.sortColumn,$scope.reverseSort);
		$scope.reverseSort = sortDataService.sortDataServiceObject.reverseSort;
		$scope.sortColumn = sortDataService.sortDataServiceObject.sortColumn;
	}

	//getting column name to sort data
	$scope.getSortClass = function (column) {
		return sortDataService.getSortClass(column,$scope.sortColumn,$scope.reverseSort);
	}

/**
 * Exports to pdf
 */
    $scope.exportpdf = function () {
    	{
    		 var item = $scope.exportInfo;
    		 item = $filter('orderBy')(item, 'sourceName');
	    	    var doc = new jsPDF();   
	    	    var col = ["Source", "Type","Created By","Last Modified At","Status"];
	    	    var rows = [];
	    	    var dateInfo,statusInfo;
	    	    for(var i=0; i<item.length;i++){
	        		var rowData = item[i];
	        		var d = new Date();
	        		d.setTime(rowData.startDate);
	        		dateInfo = moment(d).format("MMM YYYY-MM-DD HH:mm:ss ");
	        		if(rowData.status == true)
	        			statusInfo = 'Active';
	        		else
	        			statusInfo = 'Inactive';
	        		var temp = [rowData.sourceName, rowData.sourceTypes.sourceType, rowData.requestedBy,dateInfo,statusInfo];
	        		rows.push(temp);
	    	    }
    	   doc.autoTable(col, rows);
    	   doc.save('Facebook_Source'+ new Date().toDateString() +'.pdf');
    	  }
    };

});