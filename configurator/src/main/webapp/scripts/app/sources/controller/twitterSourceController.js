app.controller("twitterSourceController", function ($rootScope,$scope, $http, $state, Flash, $stateParams,$filter,exportToExcelService,formatdateService,sortDataService) {

	$scope.menu.current = "twitter";

	//sorting column 
	$scope.sortColumn = "sourceName";
	$scope.reverseSort = false;

	//OptionValues	
	$scope.rowLimitOptionArray = [10, 20, 30, 50, 100];
	$scope.selected = {
		rowLimit: $scope.rowLimitOptionArray[1]
	};

	function makeInitialTextReadOnly(input) {
		// var readOnlyLength = input.value.length;
		var readOnlyLength = 1;
		console.log('readOnlyLength', readOnlyLength);
		field.addEventListener('keydown', function (event) {
			var which = event.which;
			console.log('selectionStart ', input.selectionStart);
			if (((which == 8) && (input.selectionStart <= readOnlyLength)) ||
				((which == 46) && (input.selectionStart < readOnlyLength))) {
				event.preventDefault();
			}
		});
		field.addEventListener('keypress', function (event) {
			var which = event.which;
			if ((event.which != 0) && (input.selectionStart < readOnlyLength)) {
				event.preventDefault();
			}
		});
	}

	$scope.twittersourceTypevalidation = function (_type) {
		if (_type == 'hashtag') {
			$scope.twitterSourceName = '#';
		} else if (_type == 'handle') {
			$scope.twitterSourceName = '@';
			//makeInitialTextReadOnly(document.getElementById('field'));
		}
	}

	$scope.makeSourceNameReadonly = function (_type) {
		if (_type == 'hashtag') {
			makeInitialTextReadOnly(document.getElementById('hashtagfield'));
		} else if (_type == 'handle') {
			makeInitialTextReadOnly(document.getElementById('handlefield'));
		}
	}

	/**
	 * Sets twitter source list 
	 * response:successful rest call response conatins twitter source data
	 */
	var successCallBack = function (response) {
		$scope.twitterSourceList = response.data;
		$scope.twitterSourceList = formatdateService.formatdate(response.data);
		$scope.exportInfo = angular.copy(response.data);
	}

	/**
	 * Sets error 
	 * reason:error information
	 */
	var errorCallBack = function (reason) {
		$scope.error = reason.data;
	}

	/**
	 * Get twitter source data 
	 * status:status of source(active,inactive,all)
	 */
	getSourceList = function (status) {
		var _url;
		if (status == undefined)
			_url = 'source/twitter';
		else if (status == 'active')
			_url = 'source/twitter/active';
		else if (status == 'inactive')
			_url = 'source/twitter/inactive';
		else
			_url = 'source/twitter';
		$http({
				method: "GET",
				url: _url
			})
			.then(successCallBack, errorCallBack);
	}
	getSourceList($stateParams.status); //on load
	$scope.orderStatus = $stateParams.status;

	/**
	 * Changes state according to status
	 * status:status of source(active,inactive,all) 
	 */
	$scope.orderByStatus = function (status) {
		$state.go('homeParent.twitterSource', {
			status: $scope.orderStatus
		});
	}

	/**
	 * Resets fields from add pop up
	 */
	$scope.reset = function () {
		$scope.twitterSourceType = null;
		$scope.twitterSourceName = null;
		$scope.twitterSourcedisplayName = null;
		$scope.confirmSubmit = null;
	}

	/**
	 * Adds source
	 * _Name: source name
	 * _Type: source type
	 * _displayName: display name of twitter source
	 */
	$scope.addingSource = function (_Name, _Type, _displayName) {
		if (_Type == 'hashtag') {
			_displayName = _Name;
		}
		var twitterSource = {
			"sourceName": _Name,
			"sourceTypeName": _Type,
			"displayName": _displayName
		};
		$http({
			method: "POST",
			url: 'source/twitter',
			data: twitterSource
		}).then(function (response) {
			getSourceList($stateParams.status);
			// user role implementation messages
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _Name.toUpperCase() + '</strong> is successfully added';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _Name.toUpperCase() + '</strong> addition is pending for approval';
				var id = Flash.create('success', message);
			}
		}, function (reason) {
			var message = '<strong>Error</strong> unable to add source, might be due to duplicates.';
			var id = Flash.create('danger', message);
			$scope.error = reason.data;
		});
		$('#twitterSourceModal').modal('hide');
	}

	/**
	 * Inactivating source status
	 * _source: current source details
	 */
	$scope.inactiveStatus = function (_source) {
		var _displayName;
		_source.disabled = true;
		if (_source.sourceTypes.sourceType == 'hashtag') {
			_displayName = _source.sourceName;
		} else {
			_displayName = _source.displayName;
		}
		var twitterSource = {
			"sourceName": _source.sourceName,
			"sourceTypeName": _source.sourceTypes.sourceType,
			"displayName": _displayName
		};
		$http({
			method: "PUT",
			url: 'source/twitter/inactive/' + _source.id,
			data: twitterSource
		}).then(function (response) {
			getSourceList($stateParams.status);
			// user role implementation messages
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is succesfully deactivated';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> deactivation is pending for approval ';
				var id = Flash.create('success', message);
			}

		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is unable to deactivate';
			var id = Flash.create('danger', message);
		});
	};

	/**
	 * Activating source status
	 * _source: current source details
	 */
	$scope.activateStatus = function (_source) {
		var _displayName;
		_source.disabled = true;
		if (_source.sourceTypes.sourceType == 'hashtag') {
			_displayName = _source.sourceName;
		} else {
			_displayName = _source.displayName;
		}

		var twitterSource = {
			"sourceName": _source.sourceName,
			"sourceTypeName": _source.sourceTypes.sourceType,
			"displayName": _displayName
		};
		$http({
			method: "PUT",
			url: 'source/twitter/active/' + _source.id,
			data: twitterSource
		}).then(function (response) {
			getSourceList($stateParams.status);
			// user role implementation messages	
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is succesfully activated';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> activation is pending for approval ';
				var id = Flash.create('success', message);
			}

		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is unable to activate';
			var id = Flash.create('danger', message);
		});
	};

	//sorting column data after click on column name
	$scope.sortData = function (column) {
		sortDataService.sortData(column,$scope.sortColumn,$scope.reverseSort);
		$scope.reverseSort = sortDataService.sortDataServiceObject.reverseSort;
		$scope.sortColumn = sortDataService.sortDataServiceObject.sortColumn;
	}

	//getting column name to sort data
	$scope.getSortClass = function (column) {
		return sortDataService.getSortClass(column,$scope.sortColumn,$scope.reverseSort);
	}

	/**
	 * Exports to Excel
	 */	
    $scope.export = function(){
	    var table = $scope.exportInfo;
   	 table = $filter('orderBy')(table, 'sourceName');
    	var csvString = '<table><tr><td>Source</td><td>Display Name</td><td>Type</td><td>Created By</td><td>Last Modified At</td><td>Status</td></tr>';
	    var dateInfo,statusInfo;
    	for(var i=0; i<table.length;i++){
    		var rowData = table[i];
    		console.log(rowData);
    		var d = new Date();
    		d.setTime(rowData.startDate);
    		dateInfo = moment(d).format("MMM YYYY-MM-DD HH:mm:ss ");
    		console.log(rowData.startDate);
    		console.log(rowData.status);
    		if(rowData.status == true)
    			statusInfo = 'Active';
    		else
    			statusInfo = 'Inactive';
    		csvString = csvString + "<tr><td>" +rowData.sourceName +"</td><td>"+rowData.displayName +"</td><td>" + rowData.sourceTypes.sourceType+"</td><td>" + rowData.requestedBy+"</td><td>" +dateInfo+"</td><td>" + statusInfo+"</td>";
    		csvString = csvString + "</tr>";
	    }
    	csvString += "</table>";
     	csvString = csvString.substring(0, csvString.length);
     	exportToExcelService.converttoExcel(csvString,'Twitter_Source');	        
     }
/**
 * Exports to pdf
 */

    $scope.exportpdf = function () {
    		 var item = $scope.exportInfo;
    		 item = $filter('orderBy')(item, 'sourceName');
	    	    var doc = new jsPDF();   
	    	    var col = ["Source","Display Name" ,"Type","Created By","Last Modified At","Status"];
	    	    var rows = [];
	    	    var dateInfo,statusInfo;
	    	    for(var i=0; i<item.length;i++){
	        		var rowData = item[i];
	        		var d = new Date();
	        		d.setTime(rowData.startDate);
	        		dateInfo = moment(d).format("MMM YYYY-MM-DD HH:mm:ss ");
	        		console.log(rowData.startDate);
	        		console.log(rowData.status);
	        		if(rowData.status == true)
	        			statusInfo = 'Active';
	        		else
	        			statusInfo = 'Inactive';
	        		var temp = [rowData.sourceName,rowData.displayName, rowData.sourceTypes.sourceType, rowData.requestedBy,dateInfo,statusInfo];
	        		rows.push(temp);
	    	    }
    	   doc.autoTable(col, rows);
    	   doc.save('Twitter_Source'+ new Date().toDateString() +'.pdf');
    };

});