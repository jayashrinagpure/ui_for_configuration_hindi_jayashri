app.controller('loginController', function ($rootScope, $scope, $state, $timeout,AuthenticationService,authService ) {
	$scope.user = {};
    $scope.errors = {};
    $scope.rememberMe = true;
    $rootScope.permissions = false;
    $timeout(function () { 
       angular.element('[ng-model="username"]').focus();
   });
    $scope.login = function (credentials) {
        event.preventDefault();
        AuthenticationService.Login(credentials).then(function (response) {
        	if(credentials.username == 'admin'){
        	}
			authService.loginConfirmed();
        	$rootScope.permissions = true;
             $state.transitionTo('homeParent.home'); 
        },
        function (error) {
            $rootScope.authenticationError = true;
            console.log($rootScope.authenticationError);
            authService.loginCancelled();
        });
    };

    $scope.reset = function(){
        $scope.credentials = null;
    }
});
