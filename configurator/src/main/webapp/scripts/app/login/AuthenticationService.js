'use strict';

app
		.factory(
				'AuthenticationService',
				function($http, $rootScope, $q, localStorageService,toasty,authService) {
					'use strict';
					var service = {};

					var authenticationInfo = {
						isAuth : false,
						userName : ""
					};

					service.Login = function(credentials) {
						var usernameData = credentials.username;
						var passwordData = credentials.password;
						var data1 = {
							"username" : usernameData,
							"password" : passwordData
						};
						var deferred = $q.defer();
						//Access-Control-Expose-Headers
						
						$http({
							method : "POST",
							url : 'login',
							data : data1,
							ignoreAuthModule: 'ignoreAuthModule'
									   })
									.then(function(response,status,headers) {
							localStorageService.set('authorizationData', {
								token : response.headers('authorization'),
								userName : usernameData,
								user_role: response.headers('role')
							});
							$rootScope.globals = {
									currentUser : {
										username : usernameData,
										authdata : response.headers('authorization')
									}
								};
							$http.defaults.headers.common.Authorization = response.headers('authorization');
							var authToken = response.headers('authorization');
							//config.headers.Authorization =authToken;
							localStorageService.set('globals', $rootScope.globals);
							authenticationInfo.isAuth = true;
							authenticationInfo.userName = usernameData;
							toasty.success({
						        title: 'Success',
						        msg: 'Successfully Login!!',
						        timeout: 1500,
						        showClose: true,
						        clickToClose: true
						    });
							deferred.resolve(response);
						}, function(error) {
							$rootScope.authenticationError = true;
							toasty.error({
						        title: 'Error',
						        msg: 'Not allowed to proceed.',
						        timeout: 1500,
						        showClose: true,
						        clickToClose: true
						    });
							deferred.reject(error);
						});
						return deferred.promise;
					}
					
					service.fillAuthData = function() {
						var authData = localStorageService
								.get('authorizationData');
						if (authData) {
							authenticationInfo.isAuth = true;
							authenticationInfo.userName = authData.userName;
						}
					};

					service.ClearCredentials = function() {
						$rootScope.globals = {};
					}

					// Base64 encoding service used by AuthenticationService
					var Base64 = {

						keyStr : 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

						encode : function(input) {
							var output = "";
							var chr1, chr2, chr3 = "";
							var enc1, enc2, enc3, enc4 = "";
							var i = 0;

							do {
								chr1 = input.charCodeAt(i++);
								chr2 = input.charCodeAt(i++);
								chr3 = input.charCodeAt(i++);

								enc1 = chr1 >> 2;
								enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
								enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
								enc4 = chr3 & 63;

								if (isNaN(chr2)) {
									enc3 = enc4 = 64;
								} else if (isNaN(chr3)) {
									enc4 = 64;
								}

								output = output + this.keyStr.charAt(enc1)
										+ this.keyStr.charAt(enc2)
										+ this.keyStr.charAt(enc3)
										+ this.keyStr.charAt(enc4);
								chr1 = chr2 = chr3 = "";
								enc1 = enc2 = enc3 = enc4 = "";
							} while (i < input.length);

							return output;
						},

						decode : function(input) {
							var output = "";
							var chr1, chr2, chr3 = "";
							var enc1, enc2, enc3, enc4 = "";
							var i = 0;

							// remove all characters that are not A-Z, a-z, 0-9, +, /, or =
							var base64test = /[^A-Za-z0-9\+\/\=]/g;
							if (base64test.exec(input)) {
								window
										.alert("There were invalid base64 characters in the input text.\n"
												+ "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n"
												+ "Expect errors in decoding.");
							}
							input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

							do {
								enc1 = this.keyStr.indexOf(input.charAt(i++));
								enc2 = this.keyStr.indexOf(input.charAt(i++));
								enc3 = this.keyStr.indexOf(input.charAt(i++));
								enc4 = this.keyStr.indexOf(input.charAt(i++));

								chr1 = (enc1 << 2) | (enc2 >> 4);
								chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
								chr3 = ((enc3 & 3) << 6) | enc4;

								output = output + String.fromCharCode(chr1);

								if (enc3 != 64) {
									output = output + String.fromCharCode(chr2);
								}
								if (enc4 != 64) {
									output = output + String.fromCharCode(chr3);
								}

								chr1 = chr2 = chr3 = "";
								enc1 = enc2 = enc3 = enc4 = "";

							} while (i < input.length);

							return output;
						}
					}

					return service;
				});
