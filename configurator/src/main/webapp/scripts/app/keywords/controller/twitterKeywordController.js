app.controller("twitterKeywordController", function ($rootScope, $scope, $http, $state, Flash, $stateParams, $filter,exportToExcelService,formatdateService,sortDataService) {
	$scope.menu.current = "twitter";

	//sorting column 
	$scope.sortColumn = "keyword";
	$scope.reverseSort = false;

	//OptionValues	
	$scope.rowLimitOptionArray = [10, 20, 30, 50, 100];
	$scope.selected = {
		rowLimit: $scope.rowLimitOptionArray[1]
	}

	/**
	 * Sets twitter keyword list 
	 * response:successful rest call response conatins facebook keyword data
	 */
	var successCallBack = function (response) {
		$scope.twitterKeywordList = response.data;
		$scope.twitterKeywordList = formatdateService.formatdate(response.data);
		$scope.exportInfo = angular.copy(response.data);
	}

	/**
	 * Sets error 
	 * reason:error information
	 */
	var errorCallBack = function (reason) {
		$scope.error = reason.data;
	}

	/**
	 * Get twitter keyword data 
	 * status:status of keyword(active,inactive,all)
	 */
	getSourceList = function (status) {
		var _url;
		if (status == undefined)
			_url = 'keyword/twitter';
		else if (status == 'active')
			_url = 'keyword/twitter/active';
		else if (status == 'inactive')
			_url = 'keyword/twitter/inactive';
		else
			_url = 'keyword/twitter';
		$http({
				method: "GET",
				url: _url
			})
			.then(successCallBack, errorCallBack);
	}

	getSourceList($stateParams.status); //on load
	$scope.orderStatus = $stateParams.status;

	/**
	 * Changes state according to status
	 * status:status of keyword(active,inactive,all) 
	 */
	$scope.orderByStatus = function (status) {
		$state.go('homeParent.twitterKeyword', {
			status: $scope.orderStatus
		});
	}

	/**
	 * Resets fields from add pop up
	 */
	$scope.reset = function () {
		$scope.twitterKeywordType = null;
		$scope.twitterKeywordName = null;
		$scope.confirmSubmit = null;
	}

	/**
	 * Adds keyword
	 * _Name: keyword name
	 * _Type: keyword type
	 */
	$scope.addingKeyword = function (_Name, _Type) {
		var twitterKeyword = {
			"keyword": _Name,
			"type": _Type
		};
		$http({
				method: "POST",
				url: 'keyword/twitter',
				data: twitterKeyword
			})
			.then(function (response) {
				getSourceList($scope.orderStatus);
				// user role implementation messages
				if ($rootScope.userRole == 'super_user') {
					var message = '<strong>' + _Name.toUpperCase() + '</strong> is successfully added';
					var id = Flash.create('success', message);
				} else if ($rootScope.userRole == 'user') {
					var message = '<strong>' + _Name.toUpperCase() + '</strong> addition is pending for approval';
					var id = Flash.create('success', message);
				}
			}, function (reason) {
				var message = '<strong>Error</strong> unable to add keyword, might be due to duplicates.';
				var id = Flash.create('danger', message);
				$scope.error = reason.data;
			}); //add
		$('#twitterKeywordModal').modal('hide');
	}

	/**
	 * Inactivating keyword status
	 * _keyword: current keyword details
	 */
	$scope.inactiveStatus = function (_keyword) {
		_keyword.disabled = true
		var twitterKeyword = {
			"keyword": _keyword.keyword,
			"type": _keyword.type
		};
		$http({
			method: "PUT",
			url: 'keyword/twitter/inactive/' + _keyword.id,
			data: twitterKeyword
		}).then(function (response) {
			getSourceList($scope.orderStatus);
			// user role implementation messages
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> is succesfully deactivated';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> deactivation is pending for approval ';
				var id = Flash.create('success', message);
			}
		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> is unable to deactivate';
			var id = Flash.create('danger', message);
		});
	};

	/**
	 * Activating keyword status
	 * _keyword:current keyword details
	 */
	$scope.activateStatus = function (_keyword) {
		_keyword.disabled = true
		var twitterKeyword = {
			"keyword": _keyword.keyword,
			"type": _keyword.type
		};
		$http({
			method: "PUT",
			url: 'keyword/twitter/active/' + _keyword.id,
			data: twitterKeyword
		}).then(function (response) {
			getSourceList($stateParams.status);
			// user role implementation messages
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> is succesfully activated';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> activation is pending for approval ';
				var id = Flash.create('success', message);
			}
		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> is unable to activate';
			var id = Flash.create('danger', message);
		});
	};

	//sorting column data after click on column name
	$scope.sortData = function (column) {
		sortDataService.sortData(column,$scope.sortColumn,$scope.reverseSort);
		$scope.reverseSort = sortDataService.sortDataServiceObject.reverseSort;
		$scope.sortColumn = sortDataService.sortDataServiceObject.sortColumn;
	}

	//getting column name to sort data
	$scope.getSortClass = function (column) {
		return sortDataService.getSortClass(column,$scope.sortColumn,$scope.reverseSort);
	}


			/**
			 * Exports to Excel
			 */

		    $scope.export = function(){
			    var table = $scope.exportInfo;
		    	 table = $filter('orderBy')(table, 'keyword');
		    	var csvString = '<table><tr><td>Keyword</td><td>Type</td><td>Created By</td><td>Last Modified At</td><td>Status</td></tr>';
		    	var dateInfo,statusInfo;
		    	for(var i=0; i<table.length;i++){
		    		var rowData = table[i];
		    		var d = new Date();
		    		d.setTime(rowData.startDate);
		    		dateInfo = moment(d).format("MMM YYYY-MM-DD HH:mm:ss ");
		    		if(rowData.status == true)
		    			statusInfo = 'Active';
	        		else
	        			statusInfo = 'Inactive';
		    		csvString = csvString + "<tr><td>" +rowData.keyword + "</td><td>" + rowData.type+"</td><td>" + rowData.requestedBy+"</td><td>" + dateInfo+"</td><td>" + statusInfo+"</td>";
		    		csvString = csvString + "</tr>";
			    }
		    	csvString += "</table>";
		     	csvString = csvString.substring(0, csvString.length);
		     	exportToExcelService.converttoExcel(csvString,'Twitter_Keyword');	    
       	     }
			/**
			 * Exports to Pdf
			 */

		    $scope.exportpdf = function () {
		    		var item = $scope.exportInfo;
		    		item = $filter('orderBy')(item, 'keyword');
		    	    var doc = new jsPDF();   
		    	    var col = ["Keyword", "Type","Created By","Last Modified At","Status"];
		    	    var rows = [];
		    	    var dateInfo,statusInfo;
		    	    for(var i=0; i<item.length;i++){
		        		var rowData = item[i];
		        		var d = new Date();
		        		d.setTime(rowData.startDate);
		        		dateInfo = moment(d).format("MMM YYYY-MM-DD HH:mm:ss ");
		        		if(rowData.status == true)
		        			statusInfo = 'Active';
		        		else
		        			statusInfo = 'Inactive';
		        		var temp = [rowData.keyword,  rowData.type, rowData.requestedBy,dateInfo,statusInfo];
		        		rows.push(temp);
		    	    }
		    	   doc.autoTable(col, rows);
		    	   doc.save('Twitter_Keyword'+ new Date().toDateString() +'.pdf');
		    };
});