app.controller('homeController',function($state,$scope,localStorageService,AuthenticationService) {
	$scope.menu = {current:'home'};
//	$scope.menu.current = $state.current.name;

		$scope.message = "Home Page";
		var userName;
		var authData = localStorageService.get('authorizationData');
		if(authData == null){
       	    AuthenticationService.fillAuthData();
       	 $state.transitionTo('login');
        }
        if (authData) {
        	$scope.userName = authData.userName;
        }
	});