 app.filter("auditDateRangeFilter", function($filter) {
  return function(items, from, to, dateField) {
    startDate = moment(from).format("YYYY-MM-DD");
    endDate = moment(to).format("YYYY-MM-DD");
    if (from == '' && to == '')
    {return items;}
    return $filter('filter')(items, function(elem) {
      var actionedDate = moment(elem.actionedDate).format("YYYY-MM-DD");
      var requestedDate = moment(elem.requestedDate).format("YYYY-MM-DD");
      return actionedDate >= startDate && actionedDate <= endDate || requestedDate >= startDate && requestedDate <= endDate;
   });
  };
});